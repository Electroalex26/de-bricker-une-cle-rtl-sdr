# De-bricker une clé RTL-SDR

Programmateur d'EEPROM 24C02 avec un Arduino pour réparer une clé RTL-SDR "brickée"

Plus d'information sur l'article associé : [Débricker une clé RTL-SDR](https://www.f4iil.fr/sdr/de-bricker-une-cle-rtl-sdr/)
